﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace RenderHelper
{
    public class DiffusionEquationRenderHelper
    {
        public static Tuple<double, double> GetMinAndMaxTemperature(double[][,] layers)
        {
            double min = Double.MaxValue;
            double max = Double.MinValue;
            foreach (var layer in layers)
            {
                foreach (var x in layer)
                {
                    if (x < min)
                    {
                        min = x;
                    }
                    if (x > max)
                    {
                        max = x;
                    }
                }
            }
            return new Tuple<double, double>(min, max);
        }

        public static BitmapSource FormBitmap(double minTemperature, double maxTemperature, double[,] layer)
        {
            var xLength = layer.GetLength(0);
            var yLength = layer.GetLength(1);
            var pixels = new byte[xLength * yLength * 3];
            int pixelIndex = 0;
            for (var i = 0; i < xLength; ++i)
            {
                for (var j = 0; j < yLength; ++j)
                {
                    var color = GetColor(minTemperature, maxTemperature, layer[i, j]);
                    pixels[pixelIndex] = color.R;
                    ++pixelIndex;
                    pixels[pixelIndex] = color.G;
                    ++pixelIndex;
                    pixels[pixelIndex] = color.B;
                    ++pixelIndex;
                }
            }
            var bitMap = BitmapFrame.Create(
                xLength, yLength, 96, 96,
                PixelFormats.Rgb24, null,
                pixels, 3 * xLength
            );
            bitMap.Freeze();
            return bitMap;
        }

        private static Color GetColor(double minTemperature, double maxTemperature, double temperature)
        {
            Debug.Assert(temperature >= minTemperature);
            Debug.Assert(temperature <= maxTemperature);
            var normalizedTemperature = (temperature - minTemperature) / (maxTemperature - minTemperature);
            Debug.Assert(normalizedTemperature >= 0 && normalizedTemperature <= 1);
            var byteTemperature = Convert.ToByte(normalizedTemperature * Byte.MaxValue);
            return Color.FromRgb(byteTemperature, byteTemperature, byteTemperature);
        }
    }
}
