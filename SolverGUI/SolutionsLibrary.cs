﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.IO;

namespace SolverGUI
{
    public class SolutionsLibrary: ViewModelBase
    {
        private ObservableCollection<PrecalculatedEquationSolution> solutions;

        private PrecalculatedEquationSolution selectedItem; 
        public PrecalculatedEquationSolution SelectedItem
        {
            get { return selectedItem; }
            set { selectedItem = value; OnPropertyChanged("SelectedItem"); } 
        }

        public ObservableCollection<PrecalculatedEquationSolution> Solutions
        {
            get { return solutions; }
            private set
            { 
                solutions = value;
                solutions.CollectionChanged += (_,__) => solutions_CollectionChanged();
                solutions_CollectionChanged();
            }
        }

        private void solutions_CollectionChanged()
        {
            OnPropertyChanged("Solutions");
        }

        public SolutionsLibrary()
        {
            Solutions = new ObservableCollection<PrecalculatedEquationSolution>();
        }

        public void DeleteAllSolutions()
        {
            Solutions = new ObservableCollection<PrecalculatedEquationSolution>();
        }

        public void AddSolution(PrecalculatedEquationSolution solution)
        {
            Solutions.Add(solution);
        }

        public void SaveToFile(String filename)
        {
            var data = Solutions.ToArray();
            var serializer = new XmlSerializer(data.GetType());
            using (var textWriter = new StreamWriter(filename))
            {
                serializer.Serialize(textWriter, data);
            }
        }

        public void LoadFromFile(String filename)
        {
            var deserializer = new XmlSerializer(typeof(PrecalculatedEquationSolution[]));
            PrecalculatedEquationSolution[] array;
            using (var textReader = new StreamReader(filename))
            {
                array = (PrecalculatedEquationSolution[])deserializer.Deserialize(textReader);
            }
            DeleteAllSolutions();
            foreach (var element in array)
            {
                AddSolution(element);
            }
        }
    }
}
