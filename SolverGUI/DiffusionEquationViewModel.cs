﻿using Apex.MVVM;
using DiffusionSolver;
using RenderHelper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace SolverGUI
{
    internal class DiffusionEquationViewModel : ViewModelBase
    {
        private double maxTime;
        private double maxX;
        private double maxY;
        private ulong timeCellsAmount;
        private ulong xCellsAmount;
        private ulong yCellsAmount;
        private bool parallel;
        private BitmapSource renderedImage;
        private String timeSpentText;
        private int minLayerIndex;
        private int maxLayerIndex;
        private int currentLayerIndex;
        private bool imageIsReady;
        private AsynchronousCommand renderCommand;
        private Command saveSolutionsLibraryCommand;
        private Command loadSolutionsLibraryCommand;
        private Command clearSolutionsLibraryCommand;
        private double[][,] layers;
        private double minTemperature;
        private double maxTemperature;
        private SolutionsLibrary solutionsLibrary;

        public Command LoadSolutionFromLibraryCommand { get; private set; }

        private void loadSolutionFromLibrary()
        {
            var precalculatedSolution = TheSolutionsLibrary.SelectedItem;
            layers = precalculatedSolution.Layers;
            minTemperature = precalculatedSolution.MinTemperature;
            maxTemperature = precalculatedSolution.MaxTemperature;
            MaxTime = precalculatedSolution.MaxTime;
            MaxX = precalculatedSolution.MaxX;
            MaxY = precalculatedSolution.MaxY;
            TimeCellsAmount = precalculatedSolution.TimeCellsAmount;
            XCellsAmount = precalculatedSolution.XCellsAmount;
            YCellsAmount = precalculatedSolution.YCellsAmount;
            ImageIsReady = true;
            CurrentLayerIndex = 0;
            MinLayerIndex = 0;
            MaxLayerIndex = layers.Length - 1;
        }

        private void loadSolutionsLibrary()
        {
            var dialog = new Microsoft.Win32.OpenFileDialog();
            var result = dialog.ShowDialog();
            if (result != true)
            {
                System.Windows.MessageBox.Show("Failed to open file");
                return;
            }
            var filename = dialog.FileName;
            try
            {
                solutionsLibrary.LoadFromFile(filename);
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("Failed to load file");
            }
        }

        public Command AddSolutionToLibraryCommand
        {
            get;
            private set;
        }

        private void addSolutionToLibrary()
        {
            TheSolutionsLibrary.AddSolution(new PrecalculatedEquationSolution(
                layers, minTemperature, maxTemperature,
                MaxTime, MaxX, MaxY,
                TimeCellsAmount, XCellsAmount, YCellsAmount
            ));
        }

        private void clearSolutionsLibrary()
        {
            solutionsLibrary.DeleteAllSolutions();
        }

        private void saveSolutionsLibrary()
        {
            var dialog = new Microsoft.Win32.SaveFileDialog();
            var result = dialog.ShowDialog();
            if (result != true)
            {
                System.Windows.MessageBox.Show("Failed to create file");
                return;
            }
            var filename = dialog.FileName;
            try
            {
                solutionsLibrary.SaveToFile(filename);
            }
            catch (System.NotImplementedException exception)
            {
                System.Windows.MessageBox.Show(exception.GetType().ToString() + " " + exception.Message);
            }
        }

        public DiffusionEquationViewModel()
        {
            MaxTime = 10;
            MaxX = 10;
            MaxY = 10;
            TimeCellsAmount = 4000;
            XCellsAmount = 30;
            YCellsAmount = 30;
            Parallel = false;
            MinLayerIndex = 0;
            MaxLayerIndex = 1;
            CurrentLayerIndex = 0;
            AddSolutionToLibraryCommand = new Command(addSolutionToLibrary, false);
            ImageIsReady = false;
            renderCommand = new AsynchronousCommand(render);
            solutionsLibrary = new SolutionsLibrary();
            saveSolutionsLibraryCommand = new Command(saveSolutionsLibrary);
            loadSolutionsLibraryCommand = new Command(loadSolutionsLibrary);
            clearSolutionsLibraryCommand = new Command(clearSolutionsLibrary);
            RenderCommand.Executed += RenderCommand_Executed;
            RenderCommand.Executing += RenderCommand_Executing;
            LoadSolutionFromLibraryCommand = new Command(loadSolutionFromLibrary, false);
            TheSolutionsLibrary.PropertyChanged += TheSolutionsLibrary_PropertyChanged;
        }

        void TheSolutionsLibrary_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedItem")
            {
                var selectedItem = TheSolutionsLibrary.SelectedItem;
                LoadSolutionFromLibraryCommand.CanExecute = (
                    selectedItem != null && TheSolutionsLibrary.Solutions.Contains(selectedItem)
                );
            }
        }

        void RenderCommand_Executing(object sender, CancelCommandEventArgs args)
        {
            AddSolutionToLibraryCommand.CanExecute = false;
        }

        void RenderCommand_Executed(object sender, CommandEventArgs args)
        {
            AddSolutionToLibraryCommand.CanExecute = true;
        }

        public bool ImageIsReady
        {
            get { return imageIsReady; }
            private set
            {
                imageIsReady = value;
                OnPropertyChanged("ImageIsReady");
            }
        }

        public AsynchronousCommand RenderCommand
        {
            get { return renderCommand; }
        }

        public Command ClearSolutionsLibraryCommand
        {
            get { return clearSolutionsLibraryCommand; }
        }

        public Command LoadSolutionsLibraryCommand
        {
            get { return loadSolutionsLibraryCommand; }
        }

        public Command SaveSolutionsLibraryCommand
        {
            get { return saveSolutionsLibraryCommand; }
        }

        public int CurrentLayerIndex
        {
            get { return currentLayerIndex; }
            set
            {
                currentLayerIndex = value;
                if (layers != null)
                {
                    var currentLayer = layers[currentLayerIndex];
                    var bitMap = DiffusionEquationRenderHelper.FormBitmap(minTemperature, maxTemperature, currentLayer);
                    RenderedImage = bitMap;
                }
                OnPropertyChanged("CurrentLayerIndex");
            }
        }

        public int MinLayerIndex
        {
            get { return minLayerIndex; }
            private set { minLayerIndex = value; OnPropertyChanged("MinLayerIndex"); }
        }

        public int MaxLayerIndex
        {
            get { return maxLayerIndex; }
            private set { maxLayerIndex = value; OnPropertyChanged("MaxLayerIndex"); }
        }

        public String TimeSpent
        {
            get { return timeSpentText; }
            private set { timeSpentText = value; OnPropertyChanged("TimeSpent"); }
        }

        public BitmapSource RenderedImage
        {
            get { return renderedImage; }
            private set { renderedImage = value; OnPropertyChanged("RenderedImage"); }
        }

        public bool Parallel
        {
            get { return parallel; }
            set { parallel = value; OnPropertyChanged("Parallel"); }
        }

        public ulong TimeCellsAmount
        {
            get { return timeCellsAmount; }
            set { timeCellsAmount = value; OnPropertyChanged("TimeCellsAmount"); }
        }

        public ulong XCellsAmount
        {
            get { return xCellsAmount; }
            set { xCellsAmount = value; OnPropertyChanged("XCellsAmount"); }
        }

        public ulong YCellsAmount
        {
            get { return yCellsAmount; }
            set { yCellsAmount = value; OnPropertyChanged("YCellsAmount"); }
        }

        public double MaxTime
        {
            get { return maxTime; }
            set { maxTime = value; OnPropertyChanged("MaxTime"); }
        }

        public double MaxX
        {
            get { return maxX; }
            set { maxX = value; OnPropertyChanged("maxX"); }
        }

        public double MaxY
        {
            get { return maxY; }
            set { maxY = value; OnPropertyChanged("maxY"); }
        }

        public SolutionsLibrary TheSolutionsLibrary
        {
            get { return solutionsLibrary; }
            private set { solutionsLibrary = value; OnPropertyChanged("TheSolutionsLibrary"); }
        }

        private void render(Object obj)
        {
            Func<double, double, double> initialConditionFunction = (x, y) =>
            {
                if ((x < 2 * maxX / 3.0 && x > maxX / 3.0) && (y < 2 * maxY / 3.0 && y > maxY / 3.0))
                {
                    return 5;
                }
                return 0;
            };
            var equation = new DiffusionEquation(
                MaxX, MaxY, MaxTime, initialConditionFunction,
                TimeCellsAmount, XCellsAmount, YCellsAmount,
                Parallel
            );

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            equation.CalculateAllLayers();

            stopwatch.Stop();
            var timeElapsed = stopwatch.ElapsedMilliseconds;
            TimeSpent = Convert.ToString(timeElapsed) + " ms";

            layers = equation.Layers;
            MinLayerIndex = 0;
            MaxLayerIndex = layers.GetLength(0) - 1;
            var minAndMaxTemperature = DiffusionEquationRenderHelper.GetMinAndMaxTemperature(layers);
            minTemperature = minAndMaxTemperature.Item1;
            maxTemperature = minAndMaxTemperature.Item2;
            ImageIsReady = true;
            CurrentLayerIndex = 0;
        }
    }
}
