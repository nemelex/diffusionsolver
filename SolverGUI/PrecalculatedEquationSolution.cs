﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SolverGUI
{
    public class PrecalculatedEquationSolution
    {
        public PrecalculatedEquationSolution(
            double[][,] layers, double minTemperature, double maxTemperature,
            double maxTime, double maxX, double maxY,
            ulong timeCellsAmount, ulong xCellsAmount, ulong yCellsAmount
        )
        {
            Layers = layers;
            MinTemperature = minTemperature;
            MaxTemperature = maxTemperature;
            MaxTime = maxTime;
            MaxX = maxX;
            MaxY = maxY;
            TimeCellsAmount = timeCellsAmount;
            XCellsAmount = xCellsAmount;
            YCellsAmount = yCellsAmount;
        }
        public PrecalculatedEquationSolution() { }

        [XmlIgnore]
        public double[][,] Layers { get; set; }

        public double[][][] LayersAsJagged
        {
            get
            {
                var ZerothDimLength = Layers.Length;
                var FirstDimLength = Layers[0].GetLength(0);
                var SecondDimLength = Layers[0].GetLength(1);
                var result = new double[ZerothDimLength][][];
                for (var i = 0; i < ZerothDimLength; ++i)
                {
                    result[i] = new double[FirstDimLength][];
                    for (var j = 0; j < FirstDimLength; ++j)
                    {
                        result[i][j] = new double[SecondDimLength];
                        for (var k = 0; k < SecondDimLength; ++k)
                        {
                            result[i][j][k] = Layers[i][j, k];
                        }
                    }
                }
                return result;
            }

            set
            {
                var ZerothDimLength = value.Length;
                var FirstDimLength = value[0].Length;
                var SecondDimLength = value[0][0].Length;
                var result = new double[ZerothDimLength][,];
                for (var i = 0; i < ZerothDimLength; ++i)
                {
                    result[i] = new double[FirstDimLength, SecondDimLength];
                    for (var j = 0; j < FirstDimLength; ++j)
                    {
                        for (var k = 0; k < SecondDimLength; ++k)
                        {
                            result[i][j, k] = value[i][j][k];
                        }
                    }
                }
                Layers = result;
            }
        }

        public double MinTemperature { get; set; }

        public double MaxTemperature { get; set; }

        public double MaxTime { get; set; }

        public double MaxX { get; set; }

        public double MaxY { get; set; }

        public ulong TimeCellsAmount { get; set; }

        public ulong XCellsAmount { get; set; }

        public ulong YCellsAmount { get; set; }

        public override String ToString()
        {
            return String.Format(
                "MaxX: {0}, MaxY: {1}, MaxT: {2}, XCells: {3}, YCells: {4}, TimeCells: {5}",
                MaxX, MaxY, MaxTime, XCellsAmount, YCellsAmount, TimeCellsAmount
            );
        }
    }
}
