﻿using DiffusionSolver;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DiffusionEquationTests
{
    [TestClass]
    public class DiffusionEquationTest
    {
        [TestMethod]
        public void TestSolution()
        {
            const double maxT = 7.2;
            const double maxX = 5;
            const double maxY = 5;
            const ulong timeCellsAmount = 1000;
            const ulong xCellsAmount = 10;
            const ulong yCellsAmount = 10;
            var equation = new DiffusionEquation(
                maxX, maxY, maxT,
                (x, y) =>
                {
                    if (x < 2 || x > 3 || y < 2 || y > 3)
                    {
                        return 0;
                    }
                    return 5;
                },
                timeCellsAmount, xCellsAmount, yCellsAmount,
                false
            );
            equation.CalculateAllLayers();
            var layers = equation.Layers;
            var lastLayer = layers[layers.GetLength(0) - 1];
            var xLength = lastLayer.GetLength(0);
            var yLength = lastLayer.GetLength(1);
            for (var i = 0; i < xLength; ++i)
            {
                for (var j = 0; j < yLength; ++j)
                {
                    if (i >= 4 && i <= 6 && j >= 4 && j <= 6)
                    {
                        Assert.IsTrue(lastLayer[i, j] >= 0.005);
                        Assert.IsTrue(lastLayer[i, j] <= 0.015);
                    }
                    else
                    {
                        Assert.IsTrue(lastLayer[i, j] <= 0.005);
                        Assert.IsTrue(lastLayer[i, j] >= -0.005);
                    }
                }
            }
        }
    }
}
