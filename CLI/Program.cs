﻿using DiffusionSolver;
using System;
using System.Runtime.InteropServices;

namespace CLI
{
    internal class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            const double maxT = 7.2;
            const double maxX = 5;
            const double maxY = 5;
            const ulong timeCellsAmount = 1000;
            const ulong xCellsAmount = 10;
            const ulong yCellsAmount = 10;
            var equation = new DiffusionEquation(
                maxX, maxY, maxT,
                (x, y) =>
                {
                    if (x < 2 || x > 3 || y < 2 || y > 3)
                    {
                        return 0;
                    }
                    return 5;
                },
                timeCellsAmount, xCellsAmount, yCellsAmount,
                false
            );
            equation.CalculateAllLayers();
            Console.WriteLine("All layers were calculated");
            Console.WriteLine("Here is last layer:");
            var layers = equation.Layers;
            var lastLayer = layers[layers.GetLength(0) - 1];
            var xLength = lastLayer.GetLength(0);
            var yLength = lastLayer.GetLength(1);
            for (var i = 0; i < xLength; ++i)
            {
                for (var j = 0; j < yLength; ++j)
                {
                    Console.Write("{0:0.00}\t", lastLayer[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}
