﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiffusionSolver
{
    public class NotCalculatedException : Exception
    {
        public NotCalculatedException()
        {
        }
    }
}
