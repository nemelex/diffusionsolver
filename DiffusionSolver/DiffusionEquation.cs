﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace DiffusionSolver
{
    /// <summary>
    /// Diffusion equation defined in a rectangle
    /// Omega = {0 <= x <= l1, 0 <= y <= l2}
    /// on time interval 0 < t < maxT
    /// Boundary conditions (at x = 0 or x = maxX or y = 0 or y = maxY) are assumed to be constant
    /// meaning that they don't change over time
    /// </summary>
    public class DiffusionEquation : IApproximateXYTFunction
    {
        private readonly double h1;

        private readonly double h2;

        private readonly double maxTime;

        private readonly double omegaMaxLengthX;

        private readonly double omegaMaxLengthY;

        private readonly double tau;

        private readonly ulong timeCellsAmount;

        private readonly ulong timePointsAmount;

        private readonly Func<double, double, double> u0BoundaryFunction;

        private readonly ulong xCoordinateCellsAmount;

        private readonly ulong xCoordinatePointsAmount;

        private readonly ulong yCoordinateCellsAmount;

        private readonly ulong yCoordinatePointsAmount;
        private bool isCalculated;

        private double[][,] layers;

        private bool parallel;

        /// <summary>
        /// Initializes a new instance of the <see cref="DiffusionSolver.DiffusionEquation"/> class.
        /// </summary>
        /// <param name="l1">Upper x coordinate bound of Omega rectangle.</param>
        /// <param name="l2">Upper y coordinate bound of Omega rectangle.</param>
        /// <param name="maxT">Upper time boundary value</param>
        /// <param name="u0">Initial condition function - at t = 0.</param>
        /// <param name="timeCellsAmount">
        ///     Amount of cells used to rasterize time interval, same as K in the formula
        /// </param>
        /// <param name="xCoordinateCellsAmount">
        ///     Amount of cells used to rasterize x coordinate interval, same as N1 in the formula
        /// </param>
        /// <param name="yCoordinateCellsAmount">
        ///     Amount of cells used to rasterize y coordinate interval, same as N2 in the formula
        /// </param>
        /// <param name="parallel">Perform calculation in parallel threads or not</param>
        public DiffusionEquation(
            double l1, double l2, double maxT, Func<double, double, double> u0,
            ulong timeCellsAmount, ulong xCoordinateCellsAmount, ulong yCoordinateCellsAmount,
            bool parallel
        )
        {
            omegaMaxLengthX = l1;
            omegaMaxLengthY = l2;
            maxTime = maxT;
            u0BoundaryFunction = u0;
            this.timeCellsAmount = timeCellsAmount;
            this.xCoordinateCellsAmount = xCoordinateCellsAmount;
            this.yCoordinateCellsAmount = yCoordinateCellsAmount;
            timePointsAmount = timeCellsAmount + 1;
            xCoordinatePointsAmount = xCoordinateCellsAmount + 1;
            yCoordinatePointsAmount = yCoordinateCellsAmount + 1;
            h1 = l1 / xCoordinateCellsAmount;
            h2 = l2 / yCoordinateCellsAmount;
            tau = maxT / timeCellsAmount;
            layers = new double[timePointsAmount][,];
            this.parallel = parallel;
            isCalculated = false;
        }

        public double TimeInterval { get { return tau; } }

        public double XInterval { get { return h1; } }

        public double YInterval { get { return h2; } }

        public bool IsCalculated
        {
            get
            {
                return isCalculated;
            }
        }

        /// <summary>
        /// An array of matrices of function values.
        /// The outer array represents time layers.
        /// The inner matrice contain the function's values in coordinates
        /// defined by x cells amount and y cells amount.
        /// If the layers have not been calculated yet, this property returns an empty array.
        /// </summary>
        public double[][,] Layers
        {
            get
            {
                if (!isCalculated)
                {
                    throw new NotCalculatedException();
                }
                return layers;
            }
        }

        /// <summary>
        /// Calculates all layers.
        /// You need to use this metod before using Layers property.
        /// </summary>
        public void CalculateAllLayers()
        {
            GetLayerAtTimeIndex(timePointsAmount - 1);
            isCalculated = true;
        }

        private double CalculateCellsPointCoordinate(double max, ulong cellsAmount, ulong pointNumber)
        {
            var cellWidth = max / cellsAmount;
            return pointNumber * cellWidth;
        }

        private double CalculateFunctionAtZeroTime(double x, double y)
        {
            return u0BoundaryFunction(x, y);
        }

        private double CalculateFunctionValue(double[,] previousLayer, ulong xIndex, ulong yIndex)
        {
            Debug.Assert(layers[0] != null);
            if (
                xIndex == 0
                ||
                xIndex == xCoordinatePointsAmount - 1
                ||
                yIndex == 0
                ||
                yIndex == yCoordinatePointsAmount - 1
            )
            {
                // boundary conditions are assumed to be constant
                return layers[0][xIndex, yIndex];
            }

            var l1y = (
                previousLayer[xIndex - 1, yIndex]
                -
                2 * previousLayer[xIndex, yIndex]
                +
                previousLayer[xIndex + 1, yIndex]
            ) / (h1 * h1);
            var l2y = (
                previousLayer[xIndex, yIndex - 1]
                -
                2 * previousLayer[xIndex, yIndex]
                +
                previousLayer[xIndex, yIndex + 1]
            ) / (h2 * h2);

            return (l1y + l2y) * tau + previousLayer[xIndex, yIndex];
        }

        private double[,] CalculateLayerAtZeroTime()
        {
            var layer0 = new double[xCoordinatePointsAmount, yCoordinatePointsAmount];
            IterateOverMatrixIndices(
                (xIndex, yIndex) => layer0[xIndex, yIndex] = CalculateFunctionAtZeroTime(
                    CalculateXPointCoordinate((ulong)xIndex),
                    CalculateYPointCoordinate((ulong)yIndex)
                )
            );
            return layer0;
        }

        private double CalculateXPointCoordinate(ulong pointNumber)
        {
            return CalculateCellsPointCoordinate(omegaMaxLengthX, xCoordinateCellsAmount, pointNumber);
        }

        private double CalculateYPointCoordinate(ulong pointNumber)
        {
            return CalculateCellsPointCoordinate(omegaMaxLengthY, yCoordinateCellsAmount, pointNumber);
        }

        /// <summary>
        /// Either calculates or returns already calculated layer of function values
        /// at given time
        /// </summary>
        /// <returns>
        ///     2D array of doubles which contains values of the function at the time point
        ///     number <code>index</code>.
        /// </returns>
        /// <param name="index">
        ///     Index of the time point
        /// </param>
        private double[,] GetLayerAtTimeIndex(ulong index)
        {
            if (index == 0)
            {
                if (layers[0] == null)
                {
                    layers[0] = CalculateLayerAtZeroTime();
                }
                return layers[0];
            }
            Debug.Assert(index < timePointsAmount);
            var previousLayer = GetLayerAtTimeIndex(index - 1);
            var newLayer = new double[xCoordinatePointsAmount, yCoordinatePointsAmount];
            IterateOverMatrixIndices(
                (i, j) => newLayer[i, j] = CalculateFunctionValue(previousLayer, i, j)
            );
            layers[index] = newLayer;
            return newLayer;
        }

        private void IterateOverMatrixIndices(Action<ulong, ulong> function)
        {
            var parallelOptions = new ParallelOptions();
            if (!parallel)
            {
                parallelOptions.MaxDegreeOfParallelism = 1;
            }
            Parallel.For(
                0, Convert.ToInt32(xCoordinatePointsAmount),
                parallelOptions,
                xIndex =>
                {
                    for (ulong yIndex = 0; yIndex < yCoordinatePointsAmount; ++yIndex)
                    {
                        function((ulong)xIndex, yIndex);
                    }
                }
            );
        }
    }
}
