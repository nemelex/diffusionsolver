﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiffusionSolver
{
    public interface IApproximateXYTFunction
    {
        double TimeInterval { get; }

        double XInterval { get; }

        double YInterval { get; }

        bool IsCalculated { get; }

        /// <summary>
        /// Might throw <code>NotCalculatedException</code>
        /// </summary>
        double[][,] Layers { get; }

        void CalculateAllLayers();
    }
}
